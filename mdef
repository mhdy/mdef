#!/bin/sh

PROGRAM_NAME="`basename $0`"

EXIT_SUCCESS=0
EXIT_FAILURE=1

APIURL="https://api.dictionaryapi.dev/api/v2/entries/en/<WORD>"

usage() {
cat << EOF
Display meanings of a word.
Usage: $PROGRAM_NAME WORD
EOF
}

request() {
  word="$1"
  req="$(echo "$APIURL" | sed "s/<WORD>/$word/g")"
  curl -s "$req" > "$tmpjson"
}

jqfilter="$(cat << EOF
.[].meanings[] |
  "pos:"+.partOfSpeech,
  (
    .definitions[] |
      (
        [
          "definition:"+.definition,
          "example:"+.example,
          (["synonym:"+.synonyms[]] | @csv),
          (["antonym:"+.antonyms[]] | @csv)
        ] | @csv
      )
  )
EOF
)"

parse() {
  word="$1"
  tmpjson="$2"
  tmpcsv="$3"
  # preprocess step: convert json to csv
  grep -qs '"No Definitions Found"' "$tmpjson" && {
    echo "\n  No definitions found in english for word: $word\n"
    rm -f "$tmpjson"
    exit $EXIT_FAILURE
  }
  cat "$tmpjson" | \
    jq -r "$jqfilter" | \
    sed -e 's/"""/"/g' -e 's/"",""/","/g' -e 's/","/"|"/g' > "$tmpcsv"
  # parse csv and display meanings
  echo "\n  Meanings of the word: ${word}\n"
  i=1
  while read line
  do
    case $line in
      "pos:"*) pos="$(echo "$line" | cut -d ':' -f 2-)" ;;
      *)
        # get values
        definition="$(echo "$line" | \
          cut -d '|' -f 1 | \
          sed -e 's/definition://g' -e 's/^"//g' -e 's/"$//g')"
        example="$(echo "$line" | \
          cut -d '|' -f 2 | \
          sed -e 's/example://g' -e 's/^"//g' -e 's/"$//g')"
        words="$(echo "$line" | cut -d '|' -f 3-)"
        synonyms="$(echo "$words" | \
          tr '|' '\n' | \
          grep '^"synonym:' | \
          sed -e 's/"//g' -e 's/synonym://g')""END"
        antonyms="$(echo "$words" | \
          tr '|' '\n' | \
          grep '^"antonym:' | \
          sed -e 's/"//g' -e 's/antonym://g')""END"
        # display
        echo "    - Definition $i ($pos): $definition"
        [ -n "$(echo "$example" | sed 's/ //g')" ] \
          && echo "      Example: $example"
        [ -n "$(echo "$synonyms" | sed -e 's/ //g' -e 's/END//g')" ] && {
          echo "      Synonyms:"
          echo "$synonyms" | \
            paste -d',' - - - - - | \
            sed -e 's/^/        /g' -e 's/$/,/g' -e 's/,/, /g' -e 's/END.*//g'
        }
        [ -n "$(echo "$antonyms" | sed -e 's/ //g' -e 's/END//g')" ] && {
          echo "      Antonyms:"
          echo "$antonyms" | \
            paste -d',' - - - - -  | \
            sed -e 's/^/        /g' -e 's/$/,/g' -e 's/,/, /g' -e 's/END.*//g'
        }
        echo
        i=$((i + 1))
        ;;
    esac
  done < "$tmpcsv"
}

###
### main
###

case "$1" in
  -h|--help|"")
    usage
    exit $EXIT_SUCCESS
    ;;
  *)
    word="$1"
    tmp="$(mktemp -u)"
    tmpjson="$tmp.json"
    tmpcsv="$tmp.csv"
    request "$word"
    parse "$word" "$tmpjson" "$tmpcsv"
    # clean up
    rm -f "$tmpjson" "$tmpcsv"
    ;;
esac

exit $EXIT_SUCCESS
