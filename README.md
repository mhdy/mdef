# mdef

This script displays the meaning of the word given as argument.

It parses the JSON response returned by requesting [Free Dictionary API](https://dictionaryapi.dev/).

The script is POSIX compliant.

## Usage

```
mdef WORD
```

## Installation

Clone the repository.

```
git clone https://gitlab.com/mhdy/mdef.git
```

Then, install it using make. It will install it in `~/.local/bin/`.

```
cd mdef
make install
```

You can uninstall it by typing:

```
make uninstall
```

## License

The MIT License.
