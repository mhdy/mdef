PREFIX := ~/.local

install:
	install -m 0700 mdef $(PREFIX)/bin/

uninstall:
	rm -f $(PREFIX)/bin/mdef

.PHONY: install uninstall
